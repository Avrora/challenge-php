<?php

use App\Challenge\IoT;
use App\Challenge\Device;
use App\Challenge\Smartphone;

require_once 'vendor/autoload.php';

$computer = new Device(1, "computer Asus", "black", "192.168.1.0", true);
$toster = new Device(2, "toster", "yellow", "192.168.1.1", false);
$fridge = new Device(3, "fridge", "white", "192.168.1.2", true);
$teapot = new Device(4, "teapot", "blue", "192.168.1.3", false);

$devices = new IoT;
$devices->addDevices($computer);
$devices->addDevices($toster);
$devices->addDevices($fridge);
$devices->addDevices($teapot);
$devices->removeDevices($toster);
echo $devices->renderDevices();


$smartphone = new Smartphone("06 54 65 78 55", "Android");
echo $smartphone->renderHTML();
echo $smartphone->call("07 00 00 00 01");

echo $teapot->switchActivate($teapot);