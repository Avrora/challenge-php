<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>

    <?php

use App\Challenge\Device;
use App\Challenge\IoT;
use App\Challenge\Smartphone;

$data = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
echo "<p>Id: " . $data["id"] . "</p>";
echo "<p>Label: " . $data["label"] . "</p>";
echo "<p>Color: " . $data["color"] . "</p>";
echo "<p>IP: " . $data["ip"] . "</p>";
?>

</body>
</html>


