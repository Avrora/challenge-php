<?php

namespace App\Challenge;

class IoT {
    public $devices;

    public function __construct() {
        $this->devices = [];
    }

    public function addDevices(Device $device):void {
        array_push($this->devices, $device);
    }

    public function removeDevices(Device $device):void {
        if(($key = array_search($device, $this->devices, true)) !== FALSE) {
            unset($this->devices[$key]);
        }
    }

    public function renderDevices():string {
        $list = "<section>"; 
       
        foreach ($this->devices as $key => $device) {
            $list .= "<div>".$device->renderHTML()."</div>";
        }

        $list .= "</section>";
        return $list;
    }
}