<?php

namespace App\Challenge;

class Device {
    public $id;
    public $label;
    public $color;
    public $ip;
    public $activated;

    public function __construct(int $id, string $label, string $color, string $ip, bool $activated) {
        $this->id = $id;
        $this->label = $label;
        $this->color = $color;
        $this->ip = $ip;
        $this->activated = $activated;
    }

    public function switchActivate():string {
        if($this->activated != true) {
            $this->activated = true;
            return "<p>I turned on my device</p>";

        } else {
            $this->activated = false;
            return "<p>I turned off my device</p>";
        }
    }

    public function renderHTML():string {
        $this->label; $this->color; $this->ip;

        if($this->activated == true) {
            return "<div><p>It's a $this->label</p>
                    <p>Color: $this->color</p>
                    <p>IP: $this->ip</p></div><br/>";
        } else {
            return "$this->label is switch off";
        }   
    }

}