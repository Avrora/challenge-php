<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>A form for devices</title>
</head>
<body>
    <form action="form.devices.php" method="post">
        <p><label for="device">ID: </label>
        <input type="number" name="id" placeholder="ID of device"></p>
        <p><label for="device">Label: </label>
        <input type="text" name="label" placeholder="Label of device"></p>
        <p><label for="device">Color: </label>
        <input type="text" name="color" placeholder="Color of device"></p>
        <p><label for="device">IP: </label>
        <input type="number" name="ip" placeholder="IP of device"></p>
        <p><button>Submit</button></p>
    </form>

    <?php

use App\Challenge\Device;
use App\Challenge\IoT;
use App\Challenge\Smartphone;

// require_once 'vendor/autoload.php';

// $computer = new Device(1, "computer Asus", "black", "192.168.1.0", true);
// $toster = new Device(2, "toster", "yellow", "192.168.1.1", false);
// $fridge = new Device(3, "fridge", "white", "192.168.1.2", true);
// $teapot = new Device(4, "teapot", "blue", "192.168.1.3", false);
// $devive = new Device($data["lid"], $data["label"], $data["color"], $data["ip"], true);

$data = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);

$devicedata = new Device($data["id"], $data["label"], $data["color"], $data["ip"], true);

$devices = new IoT;
$devices->addDevices($devicedata);
$devicedata->renderHTML();

// $devices->addDevices($toster);
// $devices->addDevices($fridge);
// $devices->addDevices($teapot);
// $devices->removeDevices($toster);
// echo $devices->renderDevices();


// $smartphone = new Smartphone("06 54 65 78 55", "Android");
// echo $smartphone->renderHTML();
// echo $smartphone->call("07 00 00 00 01");

// echo $teapot->switchActivate($teapot);

// echo "<p>Id: " . $data["id"] . "</p>";
// echo "<p>Label: " . $data["label"] . "</p>";
// echo "<p>Color: " . $data["color"] . "</p>";
// echo "<p>IP: " . $data["ip"] . "</p>";

?>
</body>
</html>